(load "avl.scm")

; test of left rotation
(define avl (make-node 37))
(define test-avl (avl-insert (avl-insert (avl-insert (avl-insert (avl-insert avl 25) 87) 40) 90) 97))
(define rot-test (rotate-left test-avl))

; test of right rotation
(define avl2 (make-node 30))
(define test-avl2 (avl-insert (avl-insert (avl-insert (avl-insert (avl-insert avl2 40) 20) 10) 25) 2))
(define rot-test2 (rotate-right test-avl2))

; test of general balance function
(define test2 (balance test-avl2))
(define test1 (balance test-avl))

; avl-insert test, order: 20 45 37 80 90 27 36 15 100, results in same output as on https://www.cs.usfca.edu/~galles/visualization/AVLtree.html
(define avl (make-node 20))
(define test (avl-insert (avl-insert (avl-insert (avl-insert (avl-insert (avl-insert (avl-insert (avl-insert avl 45) 37) 80) 90) 27) 36) 15) 100))

; avl-delete test, delete: 37
(define del-test (avl-delete test 37))
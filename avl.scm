; make tree
(define (make-tree height left-child key right-child)
  (cons height (cons left-child (cons key right-child))))

; make node
(define (make-node key)
  (cons 0 (cons '() (cons key '()))))

; returns height of tree
(define (get-height tree)
  (if (null? tree)
      -1
      (car tree)))

; returns maximum of two integers
(define (get-max a b)
  (if (< a b)
      b
      a))

; return left child of tree
(define (get-left-child tree)
  (cadr tree))

; return root value of tree
(define (get-root tree)
  (caddr tree))

; return right child of tree
(define (get-right-child tree)
  (cdddr tree))

; returns if node is leaf
(define (is-leaf? node)
  (and (null? (get-left-child node)) (null? (get-right-child node))))

; returns if node has left child
(define (has-left-child? node)
  (not (null? (get-left-child node))))

; returns if node has right child
(define (has-right-child? node)
  (not (null? (get-right-child node))))

; returns balence of node
(define (get-balance node)
  (- (get-height (get-right-child node)) (get-height (get-left-child node))))

; returns minimum of tree
(define (get-min-tree tree)
  (if (has-left-child? tree)
      (get-min-tree (get-left-child tree))
      (get-root tree)))

; returns maximum of tree
(define (get-max-tree tree)
  (if (has-right-child? tree)
      (get-max-tree (get-right-child tree))
      (get-root tree)))

; checks if key is in tree
(define (in-tree? tree key)
  (cond
    ((and (< key (get-root tree)) (has-left-child? tree)) (in-tree? (get-left-child tree) key))
    ((and (> key (get-root tree)) (has-right-child? tree)) (in-tree? (get-right-child tree) key))
    ((= key (get-root tree)) #t)
    (else #f)))

; insert element in tree and keep avl property alive
(define (avl-insert tree key)
  (cond
    ((< key (get-root tree))
     (if (has-left-child? tree)
         (letrec ((temp (avl-insert (get-left-child tree) key)) ; recursive insert in left child
                  (hr (get-height (get-right-child tree))))
           (balance (make-tree
                     (+ 1 (get-max (get-height temp) hr)) temp (get-root tree) (get-right-child tree)))) ; create the new subtree and balance if necessary
         (balance (make-tree
                   (+ 1 (get-max 0 (get-height (get-right-child tree)))) (make-node key) (get-root tree) (get-right-child tree))))) ; add new leaf and balance if necessary
    ((> key (get-root tree))
     (if (has-right-child? tree)
         (letrec ((temp (avl-insert (get-right-child tree) key)) ; recursive insert in left child
                  (hl (get-height (get-left-child tree))))
           (balance (make-tree
                     (+ 1 (get-max hl (get-height temp))) (get-left-child tree) (get-root tree) temp))) ; create the new subtree and balance if necessary
         (balance (make-tree
                   (+ 1 (get-max 0 (get-height (get-left-child tree)))) (get-left-child tree) (get-root tree) (make-node key))))) ; add new leaf and balance if necessary
    (else
     (display 'elementintree))))

; delete element from tree and keep avl property alive
(define (avl-delete tree key)
  (cond
    ((= key (get-root tree))
     (cond
       ((is-leaf? tree) '())
       ((not (has-right-child? tree)) (get-left-child tree))
       ((not (has-left-child? tree)) (get-right-child tree))
       (else ; tree has left and right subtree - get the mimimum of the right subtree as new root
        (letrec ((temp (avl-delete (get-right-child tree) (get-min-tree (get-right-child tree))))
                 (new-root (get-min-tree (get-right-child tree))))
          (balance (make-tree
                    (+ 1 (get-max (get-height (get-left-child tree)) (get-height temp)))
                    (get-left-child tree) new-root temp)))))) ; create new tree and rotate if necessary
     ((< key (get-root tree))
      (letrec ((temp (avl-delete (get-left-child tree) key))) ; delete recursively in left subtree
        (balance (make-tree
                  (+ 1 (get-max (get-height temp) (get-height (get-right-child tree))))
                  temp (get-root tree) (get-right-child tree)))))
     (else
      (letrec ((temp (avl-delete (get-right-child tree) key))) ; delete recursively in right subree
        (balance (make-tree
                  (+ 1 (get-max (get-height (get-left-child tree)) (get-height temp)))
                  (get-left-child tree) (get-root tree) temp))))))
                  
; balances a tree
(define (balance tree)
  (cond
    ((= (get-balance tree) 2)
     (if (= (get-balance (get-right-child tree)) -1)
         ; if balance of right child is -1 => right rotation with right subtree, then rotate left with tree 
         (letrec ((subr (rotate-right (get-right-child tree))))
           (rotate-left (make-tree
                         (+ 1 (get-max (get-height (get-left-child tree)) (get-height subr)))
                         (get-left-child tree) (get-root tree) subr)))
         ; if balance of right child is 0, 1 => only left rotation with tree
         (rotate-left tree)))
    ((= (get-balance tree) -2)
     (if (= (get-balance (get-left-child tree)) 1)
         ; if balance of left child is 1 => left rotation with left subtree, then rotate right with tree
         (letrec ((subl (rotate-left (get-left-child tree))))
           (rotate-right (make-tree
                          (+ 1 (get-max (get-height subl) (get-height (get-right-child tree))))
                          subl (get-root tree) (get-right-child tree))))
         ; if balance of left child is 0, -1 => only right rotation with tree
         (rotate-right tree)))
    (else
     ; if tree is balanced
     tree)))
                          
; rotate left
(define (rotate-left tree)
  (letrec ((new-left
            (make-tree
             (+ 1 (get-max (get-height (get-left-child tree)) (get-height (get-left-child (get-right-child tree)))))
             (get-left-child tree) (get-root tree) (get-left-child (get-right-child tree))))
           (new-right (get-right-child (get-right-child tree))))
    (make-tree (+ 1 (get-max (get-height new-left) (get-height new-right))) new-left (get-root (get-right-child tree)) new-right)))

; rotate right
(define (rotate-right tree)
  (letrec ((new-left (get-left-child (get-left-child tree)))
           (new-right
            (make-tree
             (+ 1 (get-max (get-height (get-right-child (get-left-child tree))) (get-height (get-right-child tree))))
             (get-right-child (get-left-child tree)) (get-root tree) (get-right-child tree))))
    (make-tree (+ 1 (get-max (get-height new-left) (get-height new-right))) new-left (get-root (get-left-child tree)) new-right)))

; inorder-output
(define (inorder-output tree)
  (begin
    (if (has-left-child? tree)
        (inorder-output (get-left-child tree)))
    (display (get-root tree))
    (newline)
    (if (has-right-child? tree)
        (inorder-output (get-right-child tree)))))

; preorder-output
(define (preorder-output tree)
  (begin
    (display (get-root tree))
    (newline)
    (if (has-left-child? tree)
        (preorder-output (get-left-child tree)))
    (if (has-right-child? tree)
        (preorder-output (get-right-child tree)))))

; postorder-output
(define (postorder-output tree)
  (begin
    (if (has-left-child? tree)
        (postorder-output (get-left-child tree)))
    (if (has-right-child? tree)
        (postorder-output (get-right-child tree)))
    (display (get-root tree))
    (newline)))